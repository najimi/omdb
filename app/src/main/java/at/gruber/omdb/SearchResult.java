package at.gruber.omdb;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Markus on 22.09.2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchResult {
    @SerializedName("Search")
    public List<SearchResultMultiple> Search;
}
