package at.gruber.omdb;

import org.androidannotations.annotations.rest.Get;
import org.androidannotations.annotations.rest.Rest;
import org.androidannotations.api.rest.RestClientErrorHandling;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

/**
 * Created by Markus on 12.10.2015.
 */

@Rest(rootUrl = "http://www.omdbapi.com", converters = { MappingJackson2HttpMessageConverter.class })
public interface OmdbClient extends RestClientErrorHandling {

    @Get("/?s={search}&y=&plot=short&r=json")
    SearchResult getMovies(String search);

    @Get("/?t={title}&y=&plot=short&r=json")
    SearchResultSingle getMovie(String title);
}
