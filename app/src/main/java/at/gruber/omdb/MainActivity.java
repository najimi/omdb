package at.gruber.omdb;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.FragmentById;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.rest.RestService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import at.gruber.omdb.generated.DaoMaster;
import at.gruber.omdb.generated.DaoSession;
import at.gruber.omdb.generated.MultipleQuery;
import at.gruber.omdb.generated.MultipleQueryDao;
import at.gruber.omdb.generated.MultipleResult;
import at.gruber.omdb.generated.MultipleResultDao;
import at.gruber.omdb.generated.SingleQuery;
import at.gruber.omdb.generated.SingleQueryDao;
import at.gruber.omdb.generated.SingleResult;
import at.gruber.omdb.generated.SingleResultDao;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {

    FragmentManager fragmentManager;

    @FragmentById(R.id.fragment)
    MainActivityFragment_ mainFragment;

    @FragmentById(R.id.fragmentDetail)
    MainActivityFragmentDetail_ mainDetailFragment;

    @FragmentById(R.id.fragmentPreviousSearches)
    MainActivityFragmentPreviousSearches_ mainActivityFragmentPreviousSearches;

    Bitmap posterResult;

    @RestService
    OmdbClient restClient;

    @ViewById(R.id.button)
    Button button;

    @ViewById(R.id.button2)
    Button button2;

    @ViewById(R.id.editText)
    EditText editText;

    @ViewById(R.id.listView)
    ListView listView;

    @ViewById(R.id.listView2)
    ListView listView2;

    @ViewById(R.id.textView2)
    TextView textView2;

    @ViewById(R.id.textView3)
    TextView textView3;

    @ViewById(R.id.textView4)
    TextView textView4;

    @ViewById(R.id.imageView)
    ImageView imageView;

    SQLiteDatabase databaseConnection;
    DaoMaster daoMaster;
    DaoSession daoSession;

    SingleQueryDao singleQueryDao;
    SingleResultDao singleResultDao;
    MultipleQueryDao multipleQueryDao;
    MultipleResultDao multipleResultDao;


    @Override
    public void onBackPressed() {
        fragmentManager = getFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.show(mainFragment);
        transaction.hide(mainDetailFragment);
        transaction.hide(mainActivityFragmentPreviousSearches);
        transaction.commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        databaseConnection = new DaoMaster.DevOpenHelper(this, "omdbdb", null).getWritableDatabase();
        daoMaster = new DaoMaster(databaseConnection);
        daoSession = daoMaster.newSession();
        singleQueryDao = daoSession.getSingleQueryDao();
        singleResultDao = daoSession.getSingleResultDao();
        multipleQueryDao = daoSession.getMultipleQueryDao();
        multipleResultDao = daoSession.getMultipleResultDao();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (databaseConnection != null && databaseConnection.isOpen())
            databaseConnection.close();
    }

    private List<MultipleQuery> getMultipleQueries() {
        return multipleQueryDao.queryBuilder().listLazy();
    }

    private SingleQuery createSingleQuery(String search) {
        SingleQuery query = new SingleQuery();
        query.setSearch(search);
        singleQueryDao.insert(query);
        return query;
    }

    private SingleResult createSingleResult(SearchResultSingle singleResult, SingleQuery query) {
        SingleResult result = new SingleResult();
        result.setPoster(singleResult.Poster);
        result.setRuntime(singleResult.Runtime);
        result.setTitle(singleResult.Title);
        result.setYear(singleResult.Year);
        result.setSingleQuery(query);
        singleResultDao.insert(result);
        return result;
    }

    private MultipleQuery createMultipleQuery(String search) {
        MultipleQuery query = new MultipleQuery();
        query.setSearch(search);
        multipleQueryDao.insert(query);
        return query;
    }

    private MultipleResult createMultipleResult(SearchResultMultiple multipleResult, MultipleQuery query) {
        MultipleResult result = new MultipleResult();
        result.setTitle(multipleResult.Title);
        result.setMultipleQuery(query);
        multipleResultDao.insert(result);
        return result;
    }

    @ItemClick(R.id.listView)
    public void movieClicked(int position) {
        String title = listView.getItemAtPosition(position).toString();
        title = title.replace(' ', '+');
        searchSingle(createSingleQuery(title));
    }

    @Click(R.id.button)
    public void searchClicked(View searchButton) {
        String searchString = editText.getText().toString();
        searchString = searchString.replace(' ', '+');
        searchMovies(createMultipleQuery(searchString));
    }

    @Click(R.id.button2)
    public void previousSearchesClicked(View previousSearchesButton) {
        fragmentManager = getFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.hide(mainFragment);
        transaction.hide(mainDetailFragment);
        transaction.show(mainActivityFragmentPreviousSearches);
        transaction.commit();

        List<MultipleQuery> queries = getMultipleQueries();
        if(queries != null) {
            ArrayList<String> resultList = new ArrayList<String>();
            for(MultipleQuery query : queries) {
                resultList.add(query.getSearch());
            }
            ArrayAdapter<String> listAdapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_list_item_1, resultList);
            listView2.setAdapter(listAdapter);
        }
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            this.bmImage.setImageBitmap(result);
        }
    }

    @Background
    void searchMovies (MultipleQuery query) {
        SearchResult searchResult = restClient.getMovies(query.getSearch());
        ArrayList<String> resultList = new ArrayList<String>();
        for (SearchResultMultiple entry : searchResult.Search) {
            resultList.add(entry.Title);
            createMultipleResult(entry, query);
        }

        ArrayAdapter<String> listAdapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_list_item_1, resultList);
        updateListView(listAdapter);
    }

    @org.androidannotations.annotations.UiThread
    void updateListView(ArrayAdapter<String> listAdapter) {
        listView.setAdapter(listAdapter);
    }

    @org.androidannotations.annotations.UiThread
    void updateDetailView(SingleResult result) {
        textView2.setText("Title: " + result.getTitle());
        textView3.setText("Year: " + result.getYear());
        textView4.setText("Runtime: " + result.getRuntime());
    }

    @Background
    void searchSingle (SingleQuery query) {
        SearchResultSingle searchResult = restClient.getMovie(query.getSearch());

        fragmentManager = getFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        updateDetailView(createSingleResult(searchResult, query));
        transaction.hide(mainFragment);
        transaction.hide(mainActivityFragmentPreviousSearches);
        transaction.show(mainDetailFragment);
        new DownloadImageTask(imageView).execute(searchResult.Poster);
        transaction.commit();
    }

    private String sendSearch(String myurl) throws IOException {
        InputStream is = null;

        try {
            URL url = new URL(myurl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            int response = conn.getResponseCode();
            is = conn.getInputStream();

            String contentAsString = readIt(is);
            return contentAsString;

        } finally {
            if (is != null) {
                is.close();
            }
        }
    }

    public String readIt(InputStream stream) throws IOException, UnsupportedEncodingException {
        Reader reader = null;
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
        String line = bufferedReader.readLine();
        while(line != null){
            stringBuilder.append(line);
            stringBuilder.append('\n');
            line = bufferedReader.readLine();
        }
        return stringBuilder.toString();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
