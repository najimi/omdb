package at.gruber.omdb;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.androidannotations.annotations.EFragment;

/**
 * A placeholder fragment containing a simple view.
 */
@EFragment(R.layout.fragment_main_detail)
public class MainActivityFragmentDetail extends Fragment {

    public MainActivityFragmentDetail() {
    }
}
