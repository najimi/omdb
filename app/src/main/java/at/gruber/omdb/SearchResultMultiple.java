package at.gruber.omdb;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Markus on 20.09.2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchResultMultiple {

    @SerializedName("Title")
    public String Title;
}
