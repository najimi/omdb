package at.gruber.generator;


import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;

/**
 * Created by Markus on 13.10.2015.
 */
public class Generator {
    private static final String TARGET_FOLDER = "C:/Users/Markus/Documents/FH/MSE - 3/FDPSM/OMDb/app/src/main/java/";
    private static final int SCHEMA_VERSION = 1;

    public static final void main(String[] args) throws Throwable {
        Schema schema = new Schema(SCHEMA_VERSION, "at.gruber.omdb.generated");

        Entity multipleQuery = schema.addEntity("MultipleQuery");
        multipleQuery.setTableName("MULTIPLEQUERY");
        multipleQuery.addIdProperty();
        multipleQuery.addStringProperty("search").notNull().index();

        Entity multipleResult = schema.addEntity("MultipleResult");
        multipleResult.addIdProperty();
        multipleResult.addStringProperty("title");
        Property multipleResultId = multipleResult.addLongProperty("multipleQueryId").notNull().getProperty();
        multipleQuery.addToMany(multipleResult, multipleResultId);
        multipleResult.addToOne(multipleQuery, multipleResultId);

        Entity singleQuery = schema.addEntity("SingleQuery");
        singleQuery.setTableName("SINGLEQUERY");
        singleQuery.addIdProperty();
        singleQuery.addStringProperty("search").notNull().index();

        Entity singleResult = schema.addEntity("SingleResult");
        singleResult.addIdProperty();
        singleResult.addStringProperty("title");
        singleResult.addStringProperty("year");
        singleResult.addStringProperty("runtime");
        singleResult.addStringProperty("poster");
        Property singleResultId = singleResult.addLongProperty("singleQueryId").notNull().getProperty();
        singleQuery.addToMany(singleResult, singleResultId);
        singleResult.addToOne(singleQuery, singleResultId);

        new DaoGenerator().generateAll(schema, TARGET_FOLDER);
    }
}
